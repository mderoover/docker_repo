FROM ubuntu:18.04
RUN  apt-get update && apt-get install -y build-essential cppcheck valgrind astyle ruby avr-libc
RUN gem install ceedling
CMD bash

